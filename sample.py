from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from bs4 import BeautifulSoup as soup
import sys
import codecs 
driver = webdriver.Chrome()
driver.implicitly_wait(10)
driver.get('https://www.ebay.com/')
# Finds the search button 
searchBar = driver.find_element_by_id('gh-ac')
productName = input('Enter the name of the product that you want to get data about \n')
# Enters the user provided product name
searchBar.send_keys(productName)
# Clicks the search button 
driver.find_element_by_id('gh-btn').click()
# gets the HTML source for the page 
source = driver.page_source
page_soup = soup(source, 'html.parser')
itemNames = page_soup.findAll('h3', {'class':'s-item__title'})
itemPrices =  page_soup.findAll('span', {'class':'s-item__price'})
# For shipping 'Free Shipping' if shipping free otherwise '+$x.yy' shipping
shippingCosts =  page_soup.findAll('span', {'class':'s-item__shipping s-item__logisticsCost'})
# if the product is new or used
ownershipStatuses = page_soup.findAll('span', {'class':'SECONDARY_INFO'})
# time left to bid 
timeLeftToBidList = page_soup.findAll('span', {'class':'s-item__time-left'})
# Make a seperate file for the product that user wants information about
csvFile = open(productName + '.csv' , 'w+')
# The header for CSV file
csvHeader = 'Item Name,  Item Price, Shipping, Ownership (Used/New) \n '
csvFile.write(csvHeader)
# go through all the product listings and get info. about every product and write it to the CSV file
page_nu_max = 11
for i in range(11):
    driver.find_element_by_link_text(str(i)).click()
    print(str(i))
    try:
        
        for i in range(len(itemNames)):
    ##        print(itemNames[i].text)
    ##        print(itemPrices[i].text)
    ##        print(shippingCosts[i].text)
    ##        print(ownershipStatuses[i].text)
    ##        #print(timeLeftToBidList[i].text)
            csvFile.write(itemNames[i].text.replace(',' , '') + ',' + itemPrices[i].text.replace(',' , '') + ',' + shippingCosts[i].text.replace(',' , '') + ',' + ownershipStatuses[i].text.replace(',' , '') + '\n')
    except:
        pass 
csvFile.close()



